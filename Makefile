.PHONY: unlock todo

all: unlock todo

GIT-CRYPT := git-crypt
check-tool-git-crypt:
ifndef GIT-CRYPT
	$(error "`git-crypt` is not available please install git-crypt (https://www.agwa.name/projects/git-crypt/)")
endif

# In a real environment, DO NOT include the key in the repo
# use GPG or securely distribute the symmetric keys
unlock:
	git-crypt unlock git-crypt.key

todo: unlock
	$(MAKE) -C todo/v1.0

uninstall: unlock
	$(MAKE) -C todo/v1.0 uninstall
